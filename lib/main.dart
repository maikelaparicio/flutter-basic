import 'package:flutter/material.dart';

//void main() {
////  runApp(
////    // Text('Hola Mundo',textDirection: TextDirection.ltr,),
////    Center(
////      child: Text(
////        'Hola Mundo',
////        textDirection: TextDirection.ltr,
////      ),
////    ),
////  );
//  runApp(MyApp());
//}

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        'Hola Mundo',
        textDirection: TextDirection.ltr,
        style: TextStyle(color: Colors.blue, fontSize: 45.0, fontWeight: FontWeight.w100),
      ),
    );
  }
}
